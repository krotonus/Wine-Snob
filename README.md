This is what I made while learning Scikit-Learn.
It gives the basic overview on using the Python package to design the ML pipeline.
The script can be run to train a model to predict the Quality of the Wine.
The pickle file can be used to quickly take the trained model and predict the Quality, thereby skipping the Training phase.